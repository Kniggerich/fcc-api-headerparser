import test from 'node:test';
import assert from 'assert';
import http from 'http';

// Helper function to make GET requests
function getApi() {
    return new Promise((resolve, reject) => {
        http.get(`http://localhost:3000/api/whoami`, (res) => {
            let data = '';

            res.on('data', (chunk) => {
                data += chunk;
            });

            res.on('end', () => {
                resolve(JSON.parse(data));
            });
        }).on('error', (err) => {
            reject(err);
        });
    });
}

test('GET /api/whoami', async () => {
    const response = await getApi();
    // Check variable type
    assert.strictEqual(typeof response.ipaddress, 'string');
    assert.strictEqual(typeof response.language, 'string');
    assert.strictEqual(typeof response.software, 'string');
    // Check format for ipaddress
    assert.ok(/^(\d{1,3}\.){3}\d{1,3}$/.test(response.ipaddress));
    // Check format for language
    assert.ok(/^[a-z]{2}-[A-Z]{2}/.test(response.language));
});

