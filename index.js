// index.js
// where your node app starts

// init project
require('dotenv').config();
var express = require('express');
var app = express();

// so that your API is remotely testable by FCC
var cors = require('cors');
const {whoami} = require("./routes/whoami");
const {ipMiddleware} = require("./middleware/reqIp");
app.use(cors({ optionsSuccessStatus: 200 })); // some legacy browsers choke on 204

app.use(express.static('public'));

app.get('/', function (req, res) {
  res.sendFile(__dirname + '/views/index.html');
});

app.get('/api/whoami', ipMiddleware, whoami)

// your first API endpoint...
app.get('/api/hello', function (req, res) {
  res.json({ greeting: 'hello API' });
});

// listen for requests :)
var listener = app.listen(process.env.PORT || 3000, function () {
  console.log('Your app is listening on port ' + listener.address().port);
});
