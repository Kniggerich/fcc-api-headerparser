const requestIp = require("request-ip");

const ipMiddleware = (req, res, next) => {
    req.clientIp = requestIp.getClientIp(req);
    req.test = "abc"
    next();
}

module.exports = {
    ipMiddleware
}
