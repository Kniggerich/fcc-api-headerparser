function whoami(req, res) {

    // Extracting the IP address
    const ipaddress = req.clientIp

    // Extracting the language from the headers
    const language = req.headers['accept-language'];

    // Extracting the software (user-agent) from the headers
    const software = req.headers['user-agent'];

    // Sending the response in the specified format
    res.json({
        ipaddress: ipaddress,
        language: language.split(',')[0],  // Taking only the first part before any comma for clarity
        software: software
    });
}

module.exports = {
    whoami
}
